#!/bin/bash

function usage()
{
	echo "usage: ./map_generator.sh SIZEX SIZEY out_file.ppm"
}

function color()
{
	NPwater=$1
	NPcity=$2
	NPfire=$3

	rres=$(($RANDOM%1000))

	colored=0

	if [ $(($rres)) -ge $((0)) ] && [ $(($rres)) -lt $(($NPwater)) ]
	then
		echo "0 0 255"
		colored=1
	fi

	if [ $(($rres)) -ge $((NPwater)) ] && [ $(($rres)) -lt $(($NPwater+NPcity)) ]
	then
		echo "0 0 0"
		colored=1
	fi

	if [ $(($rres)) -ge $((NPwater+NPcity)) ] && [ $(($rres)) -lt $(($NPwater+NPcity+NPfire)) ]
	then
		echo "255 0 0"
		colored=1
	fi

	if [ "$colored" ==  "0" ]
	then
		echo "255 255 0"
	fi
}

if [ "$1" == "" ]
then
	echo "Give the map X size"
	usage
	exit
fi

if [ "$2" == "" ]
then
	echo "Give the map Y size"
	usage
	exit
fi

if [ "$3" == "" ]
then
	echo "Give out file"
	usage
	exit
fi

Pwater=20
Pcity=5
Pfire=2

if [ $((Pwater + Pcity + Pfire)) -ge $((100)) ]
then
	echo "Sum of probas must be lower than 100" 
	exit
fi

SIZEX=$1
SIZEY=$2
OUTFILE=$3

echo "P3" > $OUTFILE
echo "$SIZEX $SIZEY" >> $OUTFILE
echo "255" >> $OUTFILE

for y in $(seq 1 $SIZEY)
do
	line=""
	for x in $(seq 1 $SIZEX)
	do
		line="$line$(color $Pwater $Pcity $Pfire) "
	done
	echo "$line" >> $OUTFILE
done

