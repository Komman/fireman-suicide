# Projet d'algorithmique appliquée

## Compiler le projet

Pour compiler le projet, lancer la commande:

>>make


## Lancer le projet

Les algorithmes présentés dans le rapport sont directement sous forme de binaires dans le dossiers "algorithms".
Pour les utiliser, il suffit de les exécuter, ils prennent deux arguments en paramètre:

>>./algorithme image.ppm config.txt

Un exemple fonctionnant dans ce dépôt est (en se plaçant dans le dossier "algorithms"):

>>./reduced_maxfirst ../images/exemple1.ppm ../images/config.txt


## Les algorithmes

Les algorithmes compilés sont ceux décrits dans le rapport. Leur noms sont:

-"singletons" est l'heuristique la plus basique qui choisi tous les singletons comme pompiers.

-"maxfirst" est une heuristique choisissant de manière très peu optimisée successivement les pompiers arrêtant le plus de rayons de feu.

-"smart_maxfirst" est l'amélioration de "maxfirst" qui essaie d'empêcher un maximum les conflits entre pompiers (blocage de mêmes rayons). 

-"reduced_maxfirst" est la dernière amélioration de "smart_maxfirst" (qui reste une heuristique).

-"bruteforce" est l'algorithme testant toutes les combinaisons de pompiers de taille croissante.

-"optimforce" est l'algorithme trouvant une solution optimale en utilisant des propriétés des graphes pour gagner en rapidité. (Attention, cet algorithme est récursif et il peut facilement saturer la mémoire RAM sur des images/config trop coûteuses en calcul, il fonctionne par exemple sur "images/simple.ppm & images/config_simple.ppm"). 


## Les données

Quelques jeux de données (image et fichier config) sont présents dans le dossier "image" à la racine du dépôt.

S'il y a un problème lors de la lecture d'images, cela peut être lié au parseur d'image ppm ne supporte pas les commentaires (#...), il suffit donc de les enlever.
