#include "image.hpp"


using namespace std;

Image::Image(const std::vector<std::vector<Area>>& start_pixels)
	: _pixels(start_pixels)
{
	this->fill_specials_areas();
}


Image::Image(const std::string& path)
{
	std::ifstream file(path);
	if(!file.is_open())
	{
		err("failed to open " + path);
	}

	this->parse(file);

	file.close();
	this->fill_specials_areas();
}

const std::vector<XY>& Image::cities()
{
	return _special_areas[CITY];
}

const std::vector<XY>& Image::fire_starts()
{
	return _special_areas[FIRE];
}

const std::vector<XY>& Image::waters()
{
	return _special_areas[WATER];
}

std::vector<int> rays_in_angles(float anglemin, float anglemax)
{
	std::vector<int> ret;

	int imin = int(0.99 + anglemin / (2.0f*M_PI) * float(config::angle_count()));
	int imax = int(anglemax / (2.0f*M_PI) * float(config::angle_count()));

	for(int i=imin; i<imax; i++)
	{
		ret.push_back(i);
	}

	return ret;
}

float Image::angle_from_ray_index(int ray_index)
{
	return float(ray_index)/float(config::angle_count()) * 2.0*M_PI;
}

std::vector<int> Image::dangerous_rays(const XY& start, float anglemin, float anglemax)
{
	if(_pixels[start.x][start.y] != FIRE)
	{
		err("call to Image::compute_ray(...) on a pixel that is not a fire");
	}
	if(anglemin > anglemax)
	{
		err("call to Image::compute_ray(...) with anglemin > anglemax");
	}
	if(anglemin<0.0 || anglemin>2.0f*M_PI || anglemax<0.0 || anglemax>2.0f*M_PI)
	{
		err("call to Image::compute_ray(...): angles must be between 0.0 and 2*M_PI");
	}

	int imin = int(0.99 + anglemin / (2.0f*M_PI) * float(config::angle_count()));
	int imax = int(anglemax / (2.0f*M_PI) * float(config::angle_count()));

	if(imin<0 || imin>=config::angle_count() || imax<0 || imax>=config::angle_count())
	{
		err("Image::compute_ray(...): compute value of integer min and max failed");
	}

	std::vector<int> ret_rays;

	for(int i=imin; i<imax; i++)
	{
		if(this->compute_ray_indiced(start, i).city_burned)
		{
			ret_rays.push_back(i);
		}
	}

	return ret_rays;
}

struct XYf
{
	float x;
	float y;
};


rayResult Image::compute_ray_indiced(const XY& start, int ray_index, bool area_filling)
{
	rayResult ret;
	ret.city_burned = false;

	if(area_filling)
	{
		ret.burned_pixels.push_back(start);
	}
	
	float ray_step = 0.5;
	float ray_angle = Image::angle_from_ray_index(ray_index);
	XYf dir = {cosf(ray_angle)*ray_step, sinf(ray_angle)*ray_step};
	XYf pos = {float(start.x), float(start.y)};

	int n = _pixels.size();

	while(pos.x>=0.0 && pos.x<float(n) && pos.y>=0.0 && pos.y<float(_pixels[0].size()))
	{
		//cout<<"x : "<< int(pos.x)<<" y : "<< int(pos.y)<<endl;
		auto area = _pixels[int(pos.x)][int(pos.y)];
		if(area == WATER || area == CITY || area == FIREMAN)
		{
			ret.city_burned = (_pixels[int(pos.x)][int(pos.y)] == CITY);
			// if(area != CITY)
			// {
			// 	ret.burned_pixels.clear();
			// }
			//cout<<ret.city_burned<< int(pos.y)<<endl;
			return ret;
		}
		if(area_filling)
		{
			ret.burned_pixels.push_back({int(pos.x), int(pos.y)});
		}

		pos.x+=dir.x;
		pos.y+=dir.y;
	}

	// ret.burned_pixels.clear();
	return ret;
}

void Image::fill_specials_areas()
{
	for(unsigned int x=0; x<_pixels.size();x++)
	{
		for(unsigned int y=0; y<_pixels[x].size();y++)
		{
			_special_areas[_pixels[x][y]].push_back({(int)x, (int)y});
		}
	}
}

void Image::show(bool colored) const
{
	for(const std::vector<Area>& raw : _pixels)
	{
		for(Area a : raw)
		{
			if(colored)
			{
				cout<<COLORED_SYMBOLS[a];
			}
			cout<<SYMBOLS[a]<<" ";
			if(colored)
			{
				cout<<TERM::NOCOL;
			}
		}
		cout<<endl;
	}
}

void Image::parse(std::ifstream& file)
{
	string str;

	file>>str;
	if(str != "P3")
	{
		err("Image file must be P3 PPM format");
	}

	int size[2];
	file>>size[0]>>size[1];

	int max_pix;
	file>>max_pix;
	if(max_pix != 255)
	{
		err("Image rgb format must be maximum at 255");
	}

	_pixels.resize(size[1], std::vector<Area>(size[0], FOREST));
	Pixel p;

	for(int y=0; y<size[1];y++)
	{
		for(int x=0; x<size[0];x++)
		{
			file>>p.R>>p.G>>p.B;
			_pixels.at(y).at(x) = config::get_area(p);
		}	
	}
}

const std::vector<std::vector<Area>>& Image::pixels() const
{
	return _pixels;
}

std::vector<Area>& Image::operator[](int index)
{
	return _pixels[index];
}
const std::vector<Area>& Image::operator[](int index) const
{
	return _pixels[index];
}

void Image::save(const std::string& path)
{
	ofstream img(path);

	if(!img.is_open())
	{
		err("failed to create file: \""+path+"\"");
	}

	if(_pixels.size()==0)
	{
		err("can't create an empty img");
	}

	img<<"P3"<<endl;
	img<<_pixels.size()<<" "<<_pixels[0].size()<<endl;
	img<<"255"<<endl;
	
	Config conf = config::get(); 

	for(auto& row : _pixels)
	{
		for(auto& area : row)
		{
			Pixel pix = conf.areas_colors[area];
			img<< pix.R <<" "<< pix.G <<" "<< pix.B <<" " <<endl;	
		}
		img<<endl;
	}
	

	img.close();
}
