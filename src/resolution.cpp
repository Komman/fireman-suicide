#include "resolution.hpp"
#include "domination_algorithm.hpp"
#include "graph.hpp"
#include <unordered_map>

using namespace std;

std::vector<XY> worse_solution(const RaySetSet& rayset)
{
	std::vector<XY> ret;
	for(int i=0; i<(int)rayset.get_spots().size(); i++)
	{
		firemanSpot& spot = *(rayset.get_spots()[i].get());

		if(spot.rays.size() == 1)
		{
			ret.push_back(spot.position);
		}
	}

	return ret;
}

static int max_ray_name(const RaySetSet& rayset)
{
	int max_ray=0;

	for(int i=0; i<(int)rayset.get_spots().size(); i++)
	{
		firemanSpot& spot = *(rayset.get_spots()[i].get());

		if(spot.rays.size() == 1)
		{
			int cur_ray=0;
			for(auto& r : spot.rays)
			{
				cur_ray = r;
			}

			if(cur_ray > max_ray)
			{
				max_ray = cur_ray;
			}
		}
	}

	return max_ray;
}

static bool zero_in(const std::vector<int>& v)
{
	for(auto e : v)
	{
		if(e==0)
			return true;
	}

	return false;
}

std::vector<XY> better_than_worse_solution(RaySetSet& rayset)
{
	std::vector<XY> ret;
	int max_ray=max_ray_name(rayset);

	std::vector<int> rays_targeted(max_ray+1, -1);

	for(int i=0; i<(int)rayset.get_spots().size(); i++)
	{
		firemanSpot& spot = *(rayset.get_spots()[i].get());

		if(spot.rays.size() == 1)
		{
			for(auto& r : spot.rays)
			{
				rays_targeted[r] = 0;
			}
		}
	}


	while(zero_in(rays_targeted))
	{
		int max_size = 0;
		int max_size_index = 0;

		for(int i=0; i<(int)rayset.get_spots().size(); i++)
		{
			firemanSpot& spot = *(rayset.get_spots()[i].get());
			
			if((int)spot.rays.size() > max_size)
			{
				max_size = spot.rays.size();
				max_size_index = i;
			}
		}

		for(auto& r : rayset.get_spots()[max_size_index]->rays)
		{
			rays_targeted[r]=1;
		}

		ret.push_back(rayset.get_spots()[max_size_index]->position);
		rayset.pop_spot(max_size_index);
	}

	return ret;
}

std::vector<XY> better_than_better_than_worse_solution(RaySetSet& rayset)
{
	std::vector<XY> ret;
	int max_ray=max_ray_name(rayset);

	std::vector<int> rays_targeted(max_ray+1, -1);

	for(int i=0; i<(int)rayset.get_spots().size(); i++)
	{
		firemanSpot& spot = *(rayset.get_spots()[i].get());

		if(spot.rays.size() == 1)
		{
			for(auto& r : spot.rays)
			{
				rays_targeted[r] = 0;
			}
		}
	}


	while(zero_in(rays_targeted))
	{
		int max_size = 0;
		int max_size_index = 0;

		for(int i=0; i<(int)rayset.get_spots().size(); i++)
		{
			firemanSpot& spot = *(rayset.get_spots()[i].get());
			
			if((int)spot.rays.size() > max_size)
			{
				max_size = spot.rays.size();
				max_size_index = i;
			}
		}

		for(auto& r : rayset.get_spots()[max_size_index]->rays)
		{
			rays_targeted[r]=1;
		}

		for(auto r : rayset.get_spots()[max_size_index]->rays)
		{
			for(int i=0; i<(int)rayset.get_spots().size(); i++)
			{
				if(i != max_size_index)
				{
					firemanSpot& spot = *(rayset.get_spots()[i].get());

					spot.rays.erase(r);
				}
			}
		}

		ret.push_back(rayset.get_spots()[max_size_index]->position);
		rayset.pop_spot(max_size_index);
	}

	return ret;
}

static void dominate_raysetset_neighbourgs(RaySetSet& rayset, std::vector<int>& rays_targeted, int todom)
{
	for(auto& r : rayset.get_spots()[todom]->rays)
	{
		rays_targeted[r]=1;
	}

	for(auto r : rayset.get_spots()[todom]->rays)
	{
		for(int i=0; i<(int)rayset.get_spots().size(); i++)
		{
			if(i != todom)
			{
				firemanSpot& spot = *(rayset.get_spots()[i].get());

				spot.rays.erase(r);
			}
		}
	}
}

std::vector<XY> much_better_than_better_than_worse_solution(RaySetSet& rayset)
{
	std::vector<XY> ret;
	int max_ray=max_ray_name(rayset);

	std::vector<int> rays_targeted(max_ray+1, -1);

	for(int i=0; i<(int)rayset.get_spots().size(); i++)
	{
		firemanSpot& spot = *(rayset.get_spots()[i].get());

		if(spot.rays.size() == 1)
		{
			for(auto& r : spot.rays)
			{
				rays_targeted[r] = 0;
			}
		}
	}

	int i =0;

	while(i<(int)rayset.get_spots().size())
	{
		firemanSpot& spot = *(rayset.get_spots()[i].get());
		
		if(spot.rays.size() == 1)
		{
			rayIndex ray_id = *spot.rays.begin();
			int spot_to_dominate = -1;

			if(rays_targeted[ray_id] != 1)
			{
				int j=0;
				while(j<(int)rayset.get_spots().size())
				{
					if(j != i)
					{
						firemanSpot& spot_ifsize1 = *(rayset.get_spots()[j].get());

						if(spot_ifsize1.rays.find(ray_id) != spot_ifsize1.rays.end())
						{	
							if(spot_to_dominate == -1)
							{
								spot_to_dominate = j;
							}
							else
							{
								spot_to_dominate = -2;
								break;
							}
						}
					}
					j++;
				}
			}
			

			if(spot_to_dominate >= 0)
			{
				// cout<<"MUCH BETTER : "<<spot_to_dominate<<endl;
				dominate_raysetset_neighbourgs(rayset, rays_targeted, spot_to_dominate);

				// cout<<spot_to_dominate<<endl;
				ret.push_back(rayset.get_spots()[spot_to_dominate]->position);
				rayset.pop_spot(spot_to_dominate);
				// cout<<"Test de taille: "<<rayset.get_spots()[spot_to_dominate]->rays.size()<<endl;
				if (spot_to_dominate > i){
					i++;
				}
			}
			else 
			{
				i++;
			}
		}
		else
		{
			i++;
		}
	}


	while(zero_in(rays_targeted))
	{
		int max_size = 0;
		int max_size_index = 0;

		for(int i=0; i<(int)rayset.get_spots().size(); i++)
		{
			firemanSpot& spot = *(rayset.get_spots()[i].get());
			
			if((int)spot.rays.size() > max_size)
			{
				max_size = spot.rays.size();
				max_size_index = i;
			}
		}

		// cout<<"Dominate: "<<max_size_index<<endl;
		dominate_raysetset_neighbourgs(rayset, rays_targeted, max_size_index);

		ret.push_back(rayset.get_spots()[max_size_index]->position);
		cout<<max_size_index<<endl;
		rayset.pop_spot(max_size_index);
	}

	// for(auto f : ret)
	// {
	// 	cout<<"f("<<f.x<<", "<<f.y<<")"<<endl;
	// }


	return ret;
}

static bool next_set(std::vector<int>& dom, const RaySetSet& rayset)
{
	int i=0;
	while(true)
	{
		if(i == (int)dom.size()-1)
		{
			if(dom[i] == (int)rayset.size()-1)
			{
				dom[i] = i;
				return true;
			}
			break;
		}

		if(dom[i] < dom[i+1] - 1)
		{
			break;
		}

		dom[i] = i;
		i++;
	}

	dom[i] = dom[i] + 1;

	return false;
}

std::vector<XY> xy_from_dom(const std::vector<int>& dom, const RaySetSet& rayset)
{
	std::vector<XY> ret = {};

	for(const auto& d : dom)
	{
		// cout<<d<<": ";
		// custom_show(rayset.get_spots()[d]->rays);cout<<endl;
		ret.push_back(rayset.get_spots()[d]->position);
	}

	return ret;
}

bool dominates(const std::vector<int>& dom, const RaySetSet& origin)
{
	std::unordered_map<int, bool> dominated_rays;

	for(auto r : origin.get_rays_id())
	{
		dominated_rays[r] = false;
	}

	for(auto id : dom)
	{	
		for(auto r : origin.get_spots().at(id)->rays)
		{
			dominated_rays[r] = true;
		}
	}

	for(const auto& r : dominated_rays)
	{
		if(!(r.second))
		{
			return false;
		}
	}

	return true;
}

std::vector<XY> debile_brut_force(RaySetSet& rayset)
{
	std::vector<int> dom;
	
	for(int i=1; i<(int)rayset.size(); i++)
	{
		dom.push_back(i-1);

		cout<<"Test pour solution en "<<i<<" pompiers"<<endl;

		while(true)
		{
			// cout<<"check for: ";
			// custom_show(dom);
			// cout<<endl;
			if(dominates(dom, rayset))
			{
				return xy_from_dom(dom, rayset);
			}

			// cout<<"  before:";
			// custom_show(dom);
			// cout<<"  after:";
			if(next_set(dom, rayset))
			{
				// custom_show(dom);
				// cout<<endl;
				break;
			}
			// custom_show(dom);
			// cout<<endl;
		}
	}

	err("Wesh c'est quoi ce bazar le graphe ne se domine pas lui meme, ils est hors de ses gonds");
	return {};
}



std::vector<XY> supposed_smart_brut_force(RaySetSet& rayset)
{
	auto graphargs = rayset.to_graph();
	int Nnodes = graphargs.first.size();
	vector<int> nodes(Nnodes, 0);
	for(int i=0;i<(int)rayset.size();i++){nodes[i]=i;}
	std::vector<bool> marks(Nnodes, false);
	Graph g(nodes, marks, graphargs.second);
	// g.to_dot("graph_img.dot");

	return xy_from_dom(minimal_dominating_set(nodes, graphargs.second), rayset);
}