#include "config.hpp"

const Pixel AREAS_COLORS[AREA_COUNT] = {
	{255, 0  , 0  },
	{0  , 255, 0  },
	{0  , 0  , 0  },
	{0  , 0  , 255},
	{255, 255, 0  }
};

using namespace std;

namespace config
{
	static Config config;

	void source(const std::string& path)
	{
		ifstream file(path);
		if(!file.is_open())
		{
			err("failed to open " + path);
		}

		file >> config.angle_count;
		file >> config.fire_radius;
		file >> config.fireman_radius;

		int c;
		for(int i=0;i<AREA_COUNT;i++)
		{
			file >> c;
			file >> config.areas_colors[i].R;
			file >> config.areas_colors[i].G;
			file >> config.areas_colors[i].B;
		}

		file.close();
	}

	void show()
	{
		cout << "Config:" << std::endl;
		cout << "\tangle_count: "    << config.angle_count    << endl;
		cout << "\tfire_radius: "    << config.fire_radius    << endl;
		cout << "\tfireman_radius: " << config.fireman_radius << endl;

		for(int i=0;i<AREA_COUNT;i++)
		{
			cout << "\t" << i << ":";
			cout << " " << config.areas_colors[i].R;
			cout << " " << config.areas_colors[i].G;
			cout << " " << config.areas_colors[i].B;
			cout<<endl;
		}

	}

	int   angle_count()
	{
		return config.angle_count;
	}
	float fire_radius()
	{
		return config.fire_radius;
	}
	float fireman_radius()
	{
		return config.fireman_radius;
	}

	const Config& get()
	{
		return config;
	}

	Area get_area(const Pixel& p)
	{
		for(int i=AREA_COUNT-1;i>=0;i--)
		{
			if(config.areas_colors[i] == p)
			{
				return (Area)(i);
			}
		}
		err("Illegal pixel : (" + std::to_string(p.R) + "," 
							    + std::to_string(p.G) + ","
							    + std::to_string(p.B) + ")");
		return AREA_COUNT;
	}

	int ray_id(int fire_id, int angle_id)
	{
		return angle_id + angle_count()*fire_id;
	}

};
