#ifndef _RAY_SET_SET_HPP_
#define _RAY_SET_SET_HPP_

#include "utils.hpp"

#include <vector>
#include <unordered_set>
#include <memory>
#include <vector>

using rayIndex = int;

struct firemanSpot
{
	XY position;
	std::unordered_set<rayIndex> rays;

	void show() const;
};

class RaySetSet
{
public:
	RaySetSet();
	RaySetSet(const RaySetSet& r);

	void add_spot(const firemanSpot& spot);
	void finalize();

	void show() const;
	unsigned int size() const;

	std::pair<std::vector<XY>, std::vector<std::pair<int, int>>> to_graph();

	const std::vector<std::unique_ptr<firemanSpot>>& get_spots() const;
	const std::unordered_set<rayIndex>& get_rays_id() const;
	void pop_spot(int index);

private:

	std::vector<std::unique_ptr<firemanSpot>> _spots;
	std::unordered_set<rayIndex>              _rays_id;
};

#endif //_RAY_SET_SET_HPP_
