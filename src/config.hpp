#ifndef _CONFIG_HPP_
#define _CONFIG_HPP_

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include "utils.hpp"

enum Area {FIRE=0, FIREMAN, CITY, WATER, FOREST,
		   AREA_COUNT};

struct Pixel
{
	int R;
	int G;
	int B;

	bool operator==(const Pixel& p) const {return R == p.R && G == p.G && B == p.B;}
};

struct Config
{
	int angle_count;
	float fire_radius;
	float fireman_radius;
	Pixel areas_colors[AREA_COUNT];
};

namespace config
{
	void source(const std::string& path);
	void show();
	Area get_area(const Pixel& p);

	int   angle_count();
	float fire_radius();
	float fireman_radius();
	
	const Config& get();

	int ray_id(int fire_id, int angle_id);
};

#endif //_CONFIG_HPP_
