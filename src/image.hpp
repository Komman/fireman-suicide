#ifndef _IMAGE_HPP_
#define _IMAGE_HPP_

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include "utils.hpp"
#include "config.hpp"

const char  SYMBOLS[AREA_COUNT] = {'X', 'P', 'C', 'W', ' '};
const std::string COLORED_SYMBOLS[AREA_COUNT] = {
	TERMB::RED,
	TERMB::PURPLE,
	TERMB::WHITE,
	TERMB::BLUE,
	TERMB::GREEN
};


struct rayResult
{
	std::vector<XY> burned_pixels;
	bool city_burned;
};

class Image
{
public:
	Image(const std::string& path);
	Image(const std::vector<std::vector<Area>>& start_pixels);
	void save(const std::string& path);

	// O(1)
	const std::vector<std::vector<Area>>& pixels() const;
	virtual void show(bool colored = true) const;

	// O(1)
	std::vector<Area>& operator[](int index);
	const std::vector<Area>& operator[](int index) const;

	// O(1)
	const std::vector<XY>& cities();
	const std::vector<XY>& fire_starts();
	const std::vector<XY>& waters();

	// O(na*sqrt(n))
	std::vector<int> dangerous_rays(const XY& start, float anglemin, float anglemax);
	// O(sqrt(n))
	rayResult compute_ray_indiced(const XY& start, int ray_index, bool area_filling = false);

	// O(na)
	static std::vector<int> rays_in_angles(float anglemin, float anglemax);
	static float angle_from_ray_index(int ray_index);

protected:

	std::vector<std::vector<Area>> _pixels;

private:
	void parse(std::ifstream& file);
	void fill_specials_areas();

	std::vector<XY> _special_areas[AREA_COUNT];
};

#endif //_IMAGE_HPP_
