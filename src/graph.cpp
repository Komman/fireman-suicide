
#include "graph.hpp"

using namespace std;

// Node

// O(1)
Node::Node(int id, int name):_id(id),_name(name),_mark(false){}
Node::Node(int id, int name, bool mark):_id(id),_name(name),_mark(mark){}

// O(1)
int Node::get_id()
{
    return _id;
}

// O(1)
int Node::get_name()
{
    return _name;
}

// O(1)
void Node::add_neighbour(Node * n)
{
    _neighbour_set.push_back(n);
}

// O(1)
const vector<Node*>& Node::get_neighbours()
{
    return _neighbour_set;
}

// O(|V| : nbr vertices)
string Node::to_string()
{
    std::string s(std::to_string(this->get_name()));
    s.append(this->is_marked() ? " : o\n" : " :\n");
    for (Node * n : _neighbour_set) {
        s.append("\t");
        s.append(std::to_string(this->get_name()));
        s.append(" -- ");
        s.append(std::to_string(n->get_name()));
        s.append("\n");
    }
    return s;
}

// O(1)
void Node::set_mark(bool new_mark)
{
    _mark = new_mark;
}

// O(1)
bool Node::is_marked()
{
    return _mark;
}

// O(|V| : nbr vertices)
vector<Node*> Node::cover() {
    vector<Node*> new_marked;
    if (!this->is_marked()) {new_marked.push_back(this);}
    this->set_mark();
    for (Node * n : _neighbour_set) {
        if (!n->is_marked()) {new_marked.push_back(n);}
        n->set_mark();
    }
    return new_marked;
}

// O(|V| : nbr vertices)
int Node::get_potential_covering()
{
    int cpt = (_mark ? 0 : 1);
    for (Node * n : _neighbour_set) {
        cpt += (n->is_marked() ? 0 : 1);
    }
    return cpt;
}




// Edge

// O(1)
Edge::Edge(const string & id, Node * x, Node * z):_id(id),_x(x),_z(z){}

// O(1)
string Edge::get_id()
{
    return _id;
}

// O(1)
Node* Edge::get_first_node()
{
    return _x;
}

// O(1)
Node* Edge::get_second_node()
{
    return _z;
}

// O(1)
string Edge::to_string()
{
    std::string s (this->get_id());
    s.append(" : ");
    s.append(std::to_string(_x->get_name()));
    s.append(" -- ");
    s.append(std::to_string(_z->get_name()));
    s.append("\n");
    return s;
}

// O(1)
string Edge::to_dot() {
	return (this->get_first_node()->is_marked() ? "_" : "") + std::to_string(this->get_first_node()->get_name()) + " -- " + (this->get_second_node()->is_marked() ? "_" : "") + std::to_string(this->get_second_node()->get_name()) + " [label=\"" + this->get_id() + "\"]";
}





// Graph

// O(1)
Graph::Graph():_edge_id(-1) {}

// O(|V| + |E|)
Graph::Graph(vector<int>& names, vector<bool>& marks, vector<pair<int,int>>& edges):_edge_id(-1)
{
    for (long unsigned int i = 0; i < names.size(); ++i) {
        this->add_node(names[i], marks[i]);
    }
    for (pair<int,int> edge_pair : edges) {
        this->add_edge(edge_pair.first, edge_pair.second);
    }
}

// Graph::Graph(vector<int>& names, vector<pair<int,int>>& edges) : Graph(names, vector<bool>(names.size(),false), edges) {}

// O(|V| + |E|)
Graph::~Graph()
{
    for (Node* n : _nodes) {
        delete n;
    }
    for (Edge* e : _edges) {
        delete e;
    }
}


// O(1)
string Graph::get_new_edge_id()
{
    return std::to_string(++_edge_id);
}


// O(1)
void Graph::add_node(int name, bool mark)
{
    Node* n = new Node (_nodes.size(), name, mark);
    this->_nodes_map.insert(std::make_pair(name, _nodes.size()));
    this->_nodes.push_back(n);
}

// O(1)
void Graph::add_node(int name)
{
    this->add_node(name,false);
}

// O(1)
const vector<Node*>& Graph::get_nodes()
{
    return _nodes;
}

// O(1)
Node* Graph::get_node(int id)
{
    if ((long unsigned int) id >= _nodes.size()) {
        return NULL;
    }
    return _nodes[id];
}

// O(log(|V|))
Node* Graph::get_node_by_name(int name)
{
    auto it = _nodes_map.find(name);
    if (it == _nodes_map.end()) {
        return NULL;
    }
    return this->get_node(it->second);
}



// O(log(|V|))
Edge* Graph::add_edge(const string & name, int x_name, int z_name)
{
    Node* x = this->get_node_by_name(x_name);
    Node* z = this->get_node_by_name(z_name);
    if ((x == NULL) || (z == NULL)) {
        return NULL;
    }
    x->add_neighbour(z); z->add_neighbour(x);
    Edge* e = new Edge(name, x, z);
    _edges.push_back(e);
    return e;
}

// O(log(|V|))
Edge* Graph::add_edge(int x_name, int z_name)
{
    return add_edge(get_new_edge_id(), x_name, z_name);
}

// O(1)
const vector<Edge*>& Graph::get_edges()
{
    return _edges;
}

// O(|V|^2)
void Graph::print()
{
    for(Node * n : this->get_nodes()) {
        cout << n->to_string();
    }
    cout << endl;
}

// O(|E|)
string Graph::to_dot(string path) {
	string str = "graph {\n";
	for (Edge* t : this->get_edges()) {
		str += "\t" + t->to_dot() + "\n";
	}
    str += "}";
    if (path != "") {
        ofstream file(path);
        file << str;
        file.close();
    }
	return str;
}

// O(|V| + |E|)
Graph * Graph::copy()
{
    Graph * new_g = new Graph();
    for (long unsigned int i = 0; i < _nodes.size(); ++i) {
        new_g->add_node(_nodes[i]->get_name(), _nodes[i]->is_marked());
    }
    for (Edge * e : this->get_edges()) {
        new_g->add_edge(e->get_id(), e->get_first_node()->get_name(), e->get_second_node()->get_name());
    }
    return new_g;
}

// O(|V|)
vector<Node*> Graph::get_disk(std::vector<Node*> centers, int radius)
{
    vector<Node*> s (centers.begin(), centers.end());

    vector<Node*> to_visit (centers.begin(), centers.end());
    unordered_set<Node*> new_neighbour;
    vector<Node*> neighbor_crown (centers.begin(), centers.end());

    while ((radius > 0) && (!to_visit.empty())) {
        new_neighbour.clear();
        for (Node* n : to_visit) {
            for (Node* curr_neighbor : n->get_neighbours()) {
                new_neighbour.insert(curr_neighbor);
            }
        }
        // no duplication
        for (Node* n : neighbor_crown) {
            if (new_neighbour.find(n) != new_neighbour.end()) {
                new_neighbour.erase(n);
            }
        }
        // next to visit
        neighbor_crown.clear();
        std::copy(to_visit.begin(), to_visit.end(), back_inserter(neighbor_crown));
        std::copy(new_neighbour.begin(), new_neighbour.end(), back_inserter(neighbor_crown));
        to_visit.clear();
        std::copy(new_neighbour.begin(), new_neighbour.end(), back_inserter(to_visit));
        std::copy(new_neighbour.begin(), new_neighbour.end(), back_inserter(s));
        --radius;
    }
    return s;
}

// O(|a| + |b|)
template<typename T>
void remove_set(vector<T>& a, vector<T>& b){
    std::unordered_multiset<T> st;
    st.insert(b.begin(), b.end());
    auto predicate = [&st](const T& k){ return st.count(k) > 0; };
    a.erase(std::remove_if(a.begin(), a.end(), predicate), a.end());
}

// O(|v| : it is the parameter, not the set V of vertices of the original graph)
template<typename T>
int pos(vector<T>& v, T e) {
    for (long unsigned int i = 0; i < v.size(); ++i) {
        if (v[i] == e) {return i;}
    }
    return -1;
}
/*
Dans le parcours topologique:
- Les sommets non marqués incluent tous leurs voisins
- Les sommets marqués incluent tous leurs voisins NON MARQUÉS
*/

// O(|V|)
vector<Node*> get_marked_compo(Node* center)
{
    vector<Node*> s = {center};

    vector<Node*> to_visit = {center};
    unordered_set<Node*> new_neighbour;
    vector<Node*> neighbor_crown = {center};

    do {
        new_neighbour.clear();
        for (Node* n : to_visit) {
            for (Node* curr_neighbor : n->get_neighbours()) {
                if (!n->is_marked() || !curr_neighbor->is_marked()) {
                    new_neighbour.insert(curr_neighbor);
                }
            }
        }
        // no duplication
        for (Node* n : neighbor_crown) {
            if (new_neighbour.find(n) != new_neighbour.end()) {
                new_neighbour.erase(n);
            }
        }
        // next to visit
        neighbor_crown.clear();
        std::copy(to_visit.begin(), to_visit.end(), back_inserter(neighbor_crown));
        std::copy(new_neighbour.begin(), new_neighbour.end(), back_inserter(neighbor_crown));
        to_visit.clear();
        std::copy(new_neighbour.begin(), new_neighbour.end(), back_inserter(to_visit));
        std::copy(new_neighbour.begin(), new_neighbour.end(), back_inserter(s));
    } while (!to_visit.empty());
    return s;
}

// O(|V|^3)
vector<pair<int,int>> induced_edges(vector<Node*> node_set) {
    vector<pair<int,int>> curr_graph_edges;

    for (long unsigned int i = 0; i < node_set.size(); ++i) {
        for (Node* neighbour : node_set[i]->get_neighbours()) {
            bool encountered = false;
            int name_1 = node_set[i]->get_name(), name_2 = pos(node_set, neighbour);
            if (name_2 != -1) {
                name_2 = neighbour->get_name();
                for (pair<int,int> & p : curr_graph_edges) {
                    if (((p.first == name_1) && (p.second == name_2)) || ((p.second == name_1) && (p.first == name_2))) {
                        encountered = true;
                        break;
                    }
                }
                if (!encountered) {
                    curr_graph_edges.push_back(pair<int,int>({node_set[i]->get_name(),neighbour->get_name()}));
                }
            }
        }
    }

    return curr_graph_edges;
}

// O(|V|^3 + |E|)
vector<Graph*> Graph::get_unmarked_connected_components()
{
    vector<Node*> to_visit (this->get_nodes().begin(), this->get_nodes().end());
    vector<vector<Node*>> con_nodes;
    vector<Node*> curr_compo;

    while (!to_visit.empty()) {
        curr_compo = get_marked_compo(*(to_visit.begin()));
        if ((curr_compo.size() > 1) || !((*(curr_compo.begin()))->is_marked())) {
            con_nodes.push_back(vector<Node*> (curr_compo));
        }
        remove_set(to_visit,curr_compo);
    }

    vector<Graph*> g_set;
    vector<bool> curr_graph_marks;
    vector<int> curr_graph_names;
    vector<pair<int,int>> curr_graph_edges;

    // for each connected graph
    for (vector<Node*> node_set : con_nodes) {
        curr_graph_marks.clear(); curr_graph_names.clear(); curr_graph_edges.clear();

        for (long unsigned int i = 0; i < node_set.size(); ++i) {
            curr_graph_names.push_back(node_set[i]->get_name());
            curr_graph_marks.push_back(node_set[i]->is_marked());
        }
        curr_graph_edges = induced_edges(node_set);

        g_set.push_back(new Graph(curr_graph_names, curr_graph_marks, curr_graph_edges));
    }
    return g_set;
}

// O(|V|^2)
vector<int> Graph::cover(vector<int> to_cover)
{
    vector<int> colored;
    for (int vertex_name : to_cover) {
        for (Node* n : this->get_node_by_name(vertex_name)->cover()) {
            colored.push_back(n->get_name());
        }
    }
    return colored;
}

// O(|V|)
void Graph::revert(vector<int> to_uncolor) {
    for (int vertex_name : to_uncolor) {
        this->get_node_by_name(vertex_name)->set_mark(false);
    }
}

// O(|V|)
bool Graph::is_covered() {
    for (Node* n : _nodes) {
        if (!n->is_marked()) {
            return false;
        }
    }
    return true;
}


// Tests

// int main()
// {
//     // Graph * g = new Graph();
//     // g->add_node(1);
//     // g->add_node(2);
//     // g->add_node(3);
//     // Node * n1 = g->get_node_by_name(1);
//     // g->get_node_by_name(2);
//     // g->add_edge("e1", 1, 2);
//     // g->add_edge("e2", 1, 3);

//     // cout << "is marked :" << endl;
//     // cout << n1->is_marked() << endl;
//     // n1->set_mark();
//     // cout << n1->is_marked() << endl;

//     // cout << "get neighbour node :" << endl;
//     // for (Node * n : n1->get_neighbours()) {
//     //     cout << n->get_name() << endl;
//     // }

//     // cout << "potentiel covering :" << endl;
//     // cout << n1->get_potential_covering() << endl;
//     // cout << "covered : " << n1->cover().size() << endl;
//     // cout << n1->get_potential_covering() << endl;

//     // cout << "copy :" << endl;
//     // Graph * new_g = g->copy();
//     // new_g->print();
//     // cout << "adding new node to g :" << endl;
//     // g->add_node(4);
//     // g->add_edge("e3", 4, 1);
//     // g->print();
//     // cout << "what about new_g :" << endl;
//     // new_g->print();

//     // cout << "fast graph creation :" << endl;
//     // vector<int> names ({1,2,3});
//     // vector<bool> marks (3,false);
//     // vector<pair<int,int>> edges = { { 1 , 2 }, { 1 , 3 }, { 2 , 3 } };
//     // Graph * second_g = new Graph(names, marks, edges);
//     // second_g->print();

//     // cout << "disk test :" << endl;
//     // Graph * gra = new Graph();
//     // gra->add_node(1);
//     // Node* my_center = gra->get_node_by_name(1);
//     // gra->add_node(2);
//     // gra->add_node(3);
//     // gra->add_edge("e1", 1, 2);
//     // gra->add_edge("e2", 2, 3);
//     // // gra->add_edge("e2", 3, 1);
//     // vector<Node*> disk_set = gra->get_disk(vector<Node*> ({my_center}), 1);
//     // for(Node* n : disk_set){
//     //     cout << n->get_name() << endl;
//     // }

//     cout << "connected compo test :" << endl;
//     Graph * gra_2 = new Graph();
//     gra_2->add_node(1);
//     gra_2->add_node(2);
//     gra_2->add_node(3);
//     gra_2->add_node(4);
//     gra_2->add_node(5);
//     gra_2->add_edge("e1", 1, 2);
//     // gra_2->add_edge("e1", 2, 4);
//     gra_2->add_edge("e2", 5, 3);
//     // gra_2->add_edge("e2", 4, 3);
//     vector<Graph*> con_compo = gra_2->get_unmarked_connected_components();
//     for(Graph* my_compo : con_compo){
//         cout << "a connected component :" << endl;
//         my_compo->print();
//     }

//     // cout << "to dot test :" << endl;
//     // Graph * g_prime = new Graph();
//     // g_prime->add_node(0);
//     // g_prime->add_node(1);
//     // g_prime->add_node(2);
//     // g_prime->add_edge("e1", 0, 1);
//     // g_prime->add_edge("e2", 1, 2);
//     // g_prime->add_edge("e3", 2, 0);
//     // cout << g_prime->to_dot("./dot_file.dot") << endl;

//     // cout << "graph cover test :" << endl;
//     // Graph * g_second = new Graph();
//     // g_second->add_node(1);
//     // g_second->add_node(2);
//     // g_second->add_node(3);
//     // g_second->add_node(4);
//     // g_second->add_edge("e1", 1, 2);
//     // g_second->add_edge("e2", 2, 3);
//     // g_second->add_edge("e3", 3, 4);
//     // g_second->print();
//     // cout << "after covering :" << endl;
//     // vector<int> colored = g_second->cover((vector<int>) {2});
//     // g_second->print();
//     // cout << "pseudo reverting :" << endl;
//     // g_second->revert(vector<int> {2});
//     // g_second->print();
//     // cout << "reverting :" << endl;
//     // g_second->revert(colored);
//     // g_second->print();

//     cout << "marked connected compo test :" << endl;
//     Graph * g_ter = new Graph();
//     // First little test
//     // g_ter->add_node(0);
//     // g_ter->add_node(1);
//     // g_ter->add_node(2);
//     // g_ter->add_node(3);
//     // g_ter->add_node(4);
//     // g_ter->add_node(5);
//     // g_ter->add_node(6);
//     // g_ter->add_node(7);
//     // g_ter->add_edge("e1", 0, 1);
//     // g_ter->add_edge("e2", 1, 2);
//     // g_ter->add_edge("e3", 2, 3);
//     // g_ter->add_edge("e4", 2, 4);
//     // g_ter->add_edge("e5", 6, 5);
//     // g_ter->add_edge("e6", 6, 7);
//     // g_ter->get_node(2)->set_mark(true);
//     // g_ter->get_node(4)->set_mark(true);
//     // g_ter->get_node(6)->set_mark(true);
//     // g_ter->get_node(7)->set_mark(true);

//     // Second big test
//     g_ter->add_node(1);
//     g_ter->add_node(2);
//     g_ter->add_node(3);
//     g_ter->add_node(4);
//     g_ter->add_node(5);
//     g_ter->add_node(6);
//     g_ter->add_node(7);
//     g_ter->add_node(8);
//     g_ter->add_node(9);
//     g_ter->add_node(10);
//     g_ter->add_node(11);
//     g_ter->add_edge("", 1, 2);
//     g_ter->add_edge("", 2, 3);
//     g_ter->add_edge("", 2, 4);
//     g_ter->add_edge("", 2, 8);
//     g_ter->add_edge("", 3, 4);
//     g_ter->add_edge("", 3, 5);
//     g_ter->add_edge("", 4, 5);
//     g_ter->add_edge("", 4, 6);
//     g_ter->add_edge("", 4, 8);
//     g_ter->add_edge("", 5, 6);
//     g_ter->add_edge("", 6, 7);
//     g_ter->add_edge("", 6, 8);
//     g_ter->add_edge("", 7, 8);
//     g_ter->add_edge("", 8, 9);
//     g_ter->add_edge("", 8, 10);
//     g_ter->add_edge("", 9, 10);
//     g_ter->add_edge("", 9, 11);
//     g_ter->add_edge("", 10, 11);

//     // g_ter->get_node_by_name(2)->set_mark(true);
//     // g_ter->get_node_by_name(4)->set_mark(true);
//     // g_ter->get_node_by_name(6)->set_mark(true);
//     // g_ter->get_node_by_name(7)->set_mark(true);
//     // g_ter->get_node_by_name(8)->set_mark(true);

//     vector<Graph*> con_compo_bis = g_ter->get_unmarked_connected_components();
//     for(Graph* my_compo : con_compo_bis){
//         cout << "a connected component :" << endl;
//         my_compo->print();
//     }

//     // Deletion
//     // delete g; delete new_g; delete second_g; delete gra; delete gra_2; delete g_prime; delete g_second; delete g_ter;
//     // for (Graph* g : con_compo) {
//     //     delete g;
//     // }
//     // for (Graph* g : con_compo_bis) {
//     //     delete g;
//     // }
//     Graph * g_bis = new Graph();
//     g_bis->add_node(1);
//     g_bis->add_node(2);
//     g_bis->add_node(3);
//     g_bis->add_node(4);

//     vector<int> node_mark ({1,2,3,4});
//     g_bis->cover(node_mark);
//     cout << "Is covered : " << g_bis->is_covered() << endl;
// }
