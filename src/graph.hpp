
#include <set>
#include <algorithm>
#include <string>
#include <unordered_set>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>

#ifndef _GRAPH_HPP_
#define _GRAPH_HPP_

class Edge;

class Node
{
    std::vector<Node*> _neighbour_set;
    int _id;
    int _name;
    bool _mark;

public:
    Node(int id, int name);
    Node(int id, int name, bool mark);
    int get_id();
    int get_name();
    void add_neighbour(Node * n);
    const std::vector<Node*>& get_neighbours();
    std::string to_string();
    void set_mark(bool new_mark = true);
    bool is_marked();
    std::vector<Node*> cover();
    int get_potential_covering();
};

class Edge
{
    std::string _id;
    Node * _x;
    Node * _z;

public:
    Edge(const std::string & id, Node * x, Node * z);
    std::string get_id();
    Node* get_first_node();
    Node* get_second_node();
    std::string to_string();
    std::string to_dot();
};

class Graph
{
    std::vector<Node*> _nodes;
    std::map<int,int> _nodes_map;
    std::vector<Edge*> _edges;
    int _edge_id;

    std::string get_new_edge_id();

public:
    Graph();
    Graph(std::vector<int>& names, std::vector<bool>& marks, std::vector<std::pair<int,int>>& edges);
    // Graph(std::vector<int>& names, std::vector<std::pair<int,int>>& edges);
    ~Graph();
    void add_node(int name, bool mark);
    void add_node(int name);
    const std::vector<Node*>& get_nodes();
    Node* get_node(int id);
    Node* get_node_by_name(int name);

    Edge* add_edge(int x_id, int z_id);
    Edge* add_edge(const std::string & id, int x_id, int z_id);
    const std::vector<Edge*>& get_edges();

    void print();
    std::string to_dot(std::string path = "./dot_png_graph/graph.dot");

    Graph * copy();
    std::vector<Node*> get_disk(std::vector<Node*> centers, int radius);
    std::vector<Graph*> get_unmarked_connected_components();
    std::vector<int> cover(std::vector<int> to_cover);
    void revert(std::vector<int> to_uncolor);
    bool is_covered();
};

#endif //_GRAPH_HPP_