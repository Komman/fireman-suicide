#ifndef _RESOLUTION_HPP_
#define _RESOLUTION_HPP_

#include "utils.hpp"
#include "config.hpp"
#include "ray_set_set.hpp"

bool dominates(const std::vector<int>& dom, const RaySetSet& origin);
std::vector<XY> xy_from_dom(const std::vector<int>& dom, const RaySetSet& rayset);


std::vector<XY> worse_solution(const RaySetSet& rayset);
std::vector<XY> better_than_worse_solution(RaySetSet& rayset);
std::vector<XY> better_than_better_than_worse_solution(RaySetSet& rayset);
std::vector<XY> much_better_than_better_than_worse_solution(RaySetSet& rayset);
std::vector<XY> debile_brut_force(RaySetSet& rayset);
std::vector<XY> supposed_smart_brut_force(RaySetSet& rayset);

enum ALGORITHMS {SINGLETONS       = 0,
				 MAXFIRST         = 1,
				 SMART_MAXFIRST   = 2,
				 REDUCED_MAXFIRST = 3,
				 BRUTEFORCE       = 4,
				 OPTIMFORCE       = 5};

inline std::vector<XY> algorithm(RaySetSet& rayset)
{

	if constexpr      (CHOOSEN_ALGORITHM == SINGLETONS) 
	{
		return worse_solution(rayset);
	}
	else if constexpr (CHOOSEN_ALGORITHM == MAXFIRST)
	{
		return better_than_worse_solution(rayset);
	}
	else if constexpr (CHOOSEN_ALGORITHM == SMART_MAXFIRST)
	{
		return better_than_better_than_worse_solution(rayset);
	}
	else if constexpr (CHOOSEN_ALGORITHM == REDUCED_MAXFIRST)
	{
		return much_better_than_better_than_worse_solution(rayset);
	}
	else if constexpr (CHOOSEN_ALGORITHM == BRUTEFORCE)
	{
		return debile_brut_force(rayset);
	}
	else if constexpr (CHOOSEN_ALGORITHM == OPTIMFORCE)
	{
		return supposed_smart_brut_force(rayset);
	}
	else
	{
		// PAS CENSE COMPILER CETTE LIGNE
	}

}


#endif //_RESOLUTION_HPP_
