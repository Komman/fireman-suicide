
#include <vector>

std::vector<int> minimal_dominating_set(std::vector<int> nodes, std::vector<std::pair<int, int>> edges);