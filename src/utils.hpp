#ifndef _UTILS_HPP_
#define _UTILS_HPP_

#include <string>
#include <fstream>
#include <iostream>
#include <math.h>
#include <vector>
#include <unordered_set>

template<typename T>
void custom_show(const T& v)
{
	std::cout<<"{";
	for(const auto& e :v)
	{
		std::cout<<e<<", ";
	}
	std::cout<<"}";
}

struct XY
{
	int x;
	int y;

	friend std::ostream& operator<<(std::ostream& os, const XY& xy)
	{
		os<<"(x: "<<xy.x<<", y: "<<xy.y<<")";

		return os;
	}
};


namespace TERMB
{
	const std::string BLACK  = "\033[7;30m";
	const std::string RED    = "\033[7;31m";
	const std::string GREEN  = "\033[7;32m";
	const std::string ORANGE = "\033[7;33m";
	const std::string BLUE   = "\033[7;34m";
	const std::string PURPLE = "\033[7;35m";
	const std::string CYAN   = "\033[7;36m";
	const std::string WHITE  = "\033[7;37m";
	const std::string NOCOL  = "\033[0m";
};
namespace TERM
{
	const std::string BLACK  = "\033[1;30m";
	const std::string RED    = "\033[1;31m";
	const std::string GREEN  = "\033[1;32m";
	const std::string ORANGE = "\033[1;33m";
	const std::string BLUE   = "\033[1;34m";
	const std::string PURPLE = "\033[1;35m";
	const std::string CYAN   = "\033[1;36m";
	const std::string WHITE  = "\033[1;37m";
	const std::string NOCOL  = "\033[0m";
};

inline void err(const std::string& err_msg)
{
	std::cout<<"Error: "<<err_msg<<std::endl;
	exit(-1);
}

template<typename T>
inline void print_vector(const std::vector<T>& v)
{
	std::cout<<"{";
	for(const auto& e : v)
	{
		std::cout<<e<<", ";
	}
	std::cout<<"}"<<std::endl;
}
template<typename T>
inline void print_unordered_set(const std::unordered_set<T>& v)
{
	std::cout<<"{";
	for(const auto& e : v)
	{
		std::cout<<e<<", ";
	}
	std::cout<<"}"<<std::endl;
}

#endif //_UTILS_HPP_
