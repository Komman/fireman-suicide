#ifndef _PROPAGATION_HPP_
#define _PROPAGATION_HPP_

#include "image.hpp"
#include "utils.hpp"
#include "ray_set_set.hpp"

class Propagation : public Image
{
public:
	Propagation(const Image& i);

	virtual void show(bool colored = true) const override;

	void prepare_fireman(RaySetSet& sets);

	void burn_image();

private:
	std::unordered_set<rayIndex> get_critical_rays(const XY& position);
	void propagate();
	void set_fireman_action();
	void set_fire_action();

	struct processedInfos
	{
		bool burning;
		bool fireman;
		std::vector<int> ray_passed;
		bool fire_ranged;
	};

	std::vector<std::vector<processedInfos>> _burning;
};

#endif //_PROPAGATION_HPP_
