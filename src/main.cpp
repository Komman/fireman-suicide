#include <iostream>

#include "image.hpp"
#include "propagation.hpp"
#include "ray_set_set.hpp"
#include "graph.hpp"
#include "resolution.hpp"
#include "domination_algorithm.hpp"
#include <chrono>
#include <unistd.h>

using namespace std::chrono;
using namespace std;

uint64_t timeSinceEpochMillisec() {
  return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}


int main(int argc, char* argv[])
{
	string config_file;
	string img_file;
	if(argc <= 2)
	{
		cout<<TERM::RED<<"Not enough arguments. Usage:"<<endl;
		cout<<TERM::PURPLE<<"./algorithm "<<TERM::ORANGE<<"image.ppm "<<TERM::BLUE<<"config.txt"<<TERM::NOCOL<<endl;
		return 0;
	}
	
	img_file = string(argv[1]);
	config_file = string(argv[2]);

	cout<<"Config file: "<<config_file<<endl;
	config::source(config_file);
	config::show();

	Image i(img_file);

	i.show();

	Propagation p(i);

	cout<<endl;
	p.show();

	RaySetSet ss;

	p.prepare_fireman(ss);
	// ss.show();		
	cout<<"size: "<<ss.get_spots().size()<<endl;

	
	cout<<"Start algorithm !"<<endl;

	uint64_t before = timeSinceEpochMillisec();

	auto firemans = algorithm(ss);

	uint64_t after = timeSinceEpochMillisec();

	cout<<"Set found !"<<endl;



	// ss.show();

	for(auto& f : firemans)
	{
		// cout<<"POSITION: X:"<<f.x<<"\tY: "<<f.y<<endl;
		i[f.x][f.y] = FIREMAN;
	}
	Propagation ppost(i);

	cout<<endl;
	ppost.show();

	cout<<endl<<"Nombre de Pompiers utilisés: "<<TERM::ORANGE<<firemans.size()<<TERM::NOCOL<<endl;
	cout<<"Temps: "<<TERM::BLUE<<(double)(after - before)/(double)(1000.0)<<TERM::NOCOL<<" s"<<endl;

	ppost.burn_image();
	// ppost.save("oui.ppm");

	return 0;
}
