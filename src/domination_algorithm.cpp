
#include "domination_algorithm.hpp"
#include "graph.hpp"

using namespace std;


vector<int> aux(Graph* graph, long unsigned int maximum_stack_length);


bool compareNodes(Node* node1, Node* node2) {
    return node1->get_potential_covering() > node2->get_potential_covering();
}


void aux2(Graph* graph, vector<int>& stack, vector<int>& result, vector<bool>& explored_roots, long unsigned int maximum_stack_length)
{
    vector<Graph*> unmarked_components = graph->get_unmarked_connected_components();
    if (unmarked_components.size() > 1) {
        vector<int> r(stack);

        for (long unsigned int i = 0; i < unmarked_components.size(); ++i) {
            if (r.size() < maximum_stack_length && r.size() < result.size()) {
                vector<int> dominating = aux(unmarked_components[i], min(maximum_stack_length, result.size()) - r.size());
                copy(dominating.begin(), dominating.end(), back_inserter(r));
            }
        }

        if (r.size() < maximum_stack_length && r.size() < result.size()) {
            result = r;
        }

        return;
    }

    for (Graph* g : unmarked_components) {
        delete g;
    }

    if (stack.size() >= maximum_stack_length || stack.size() >= result.size())
        return;

    vector<Node*> nodes = graph->get_nodes();
    int uncovered = nodes.size();

    for (Node* n : nodes) {
        uncovered -= (n->is_marked()) ? 1 : 0;
    }

    if (uncovered <= 0) {
        result = stack;
    }

    if (stack.size() >= maximum_stack_length - 1 || stack.size() >= result.size() - 1)
        return;

    vector<Node*> stack_vertices;
    for (int v : stack) {
        stack_vertices.push_back(graph->get_node_by_name(v));
    }
    vector<Node*> important_vertices = (stack.size() > 0) ? graph->get_disk(stack_vertices, 3) : nodes;
    vector<Node*> remaining_vertices;
    for (Node* v : important_vertices) {
        if (find(stack.begin(), stack.end(), v->get_name()) == stack.end()) {
            if (v->get_potential_covering() >= 1) {
                if (!explored_roots[v->get_name()]) {
                    if (stack.size() + 1 == maximum_stack_length - 1 || stack.size() + 1 == result.size() - 1) {
                        if (v->get_potential_covering() >= uncovered)
                            remaining_vertices.push_back(v);
                    } else {
                        remaining_vertices.push_back(v);
                    }
                }
            }
        }
    }

    sort(remaining_vertices.begin(), remaining_vertices.end(), compareNodes);

    for (Node* vertex : remaining_vertices) {
        vector<int> to_cover(1, vertex->get_name());
        vector<int> modif = graph->cover(to_cover);
        vector<int> stack_ = stack;
        stack_.push_back(vertex->get_name());

        aux2(
            graph,
            stack_,
            result,
            explored_roots,
            maximum_stack_length
        );

        graph->revert(modif);

        if (stack.size() == 0) {
            explored_roots[vertex->get_name()] = true;
        }

        if (stack.size() >= maximum_stack_length - 1 || stack.size() >= result.size() - 1) return;
    }
}


vector<int> aux(Graph* graph, long unsigned int maximum_stack_length)
{
    vector<int> ids;
    for (Node* n : graph->get_nodes()) {
        ids.push_back(n->get_name());
    }
    vector<int> dominating(ids);
    vector<int> stack;
    vector<bool> explored_roots(graph->get_nodes().size(), false);

    aux2(graph, stack, dominating, explored_roots, maximum_stack_length);

    return dominating;
}


vector<int> minimal_dominating_set(vector<int> nodes, vector<pair<int, int>> edges)
{
    vector<bool> marks(nodes.size(), false);

    Graph* graph = new Graph(nodes, marks, edges);

    vector<int> result = aux(graph, nodes.size());

    delete graph;
    return result;
}





// Pseudo code pour la nouvelle fonction
// solution_set rec_get_solution(remaining_vertices, current_set, len) {
//     if (len == 0) {
//         if (graph.dominated(current_set)) {
//             return current_set;
//         } else {
//             return []:
//         }
//     }

//     for (vertex : remaining_vertices) {
//         result = rec_get_solution(remaining_vertices - vertex, current_set + vertex, len -1);
//         if (result != []) {
//             return result;
//         }
//     }
//     return [];
// }

// solution_set dichoto_find(heurist_set) {
//     int a = 0, b = heurist_set.size;
//     current_res = heurist_set;
//     while (a <= b) {
//         int m = (a+b)/2;
//         res = rec_get_solution(graph.vertices, [], m);
//         if (rec == []) {
//             a = m + 1;
//         } else {
//             current_res = res;
//             b = m - 1;
//         }
//     }
//     return current_res;
// }