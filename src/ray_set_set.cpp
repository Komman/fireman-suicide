#include "ray_set_set.hpp"

#include <algorithm>
#include <functional>


using namespace std;

RaySetSet::RaySetSet()
{

}

RaySetSet::RaySetSet(const RaySetSet& r)
	: _spots(),
	  _rays_id(r._rays_id)
{
	for(const auto& spot : r._spots)
	{
		_spots.push_back(std::make_unique<firemanSpot>(*spot));
	}
}

const std::unordered_set<rayIndex>& RaySetSet::get_rays_id() const
{
	return _rays_id;
}


template<typename T>
static void swapop(vector<T>& v, int index)
{
	if(index > (int)v.size() -1)
	{
		err("Le sang de tes morts les bornes du vecteur respectes les, wesh (ne pas pretter attentino à ce message durant la correction de ce projet, ce n'est pas censé arrviver)");
	}

	if(index < (int)v.size() -1)
	{
		v[index] = std::move(v[v.size() -1]);
	}

	v.pop_back();
}

void RaySetSet::add_spot(const firemanSpot& spot)
{
	// cout<<"=========="<<endl;
	// spot.show();cout<<" <== "<<endl;
	
	std::vector<int> to_replace_id;

	for(int i=0; i<(int)_spots.size(); i++)
	{
		auto& self_spot = *(_spots[i].get());

		const firemanSpot* smaller;
		const firemanSpot* upper;
		bool self_taken;
		
		if(self_spot.rays.size() < spot.rays.size())
		{
			self_taken = true;
			smaller = &self_spot;
			upper = &spot;
		}
		else
		{
			self_taken = false;
			smaller = &spot;
			upper   = &self_spot;
		}

		bool local_included = true;
		bool spot_included  = false;
		for(const auto& ray : smaller->rays)
		{
			// cout<<endl;
			// upper->show();
			// cout<<endl<<ray<<endl;
			if(upper->rays.find(ray) == upper->rays.end())
			{
				// cout<<"not included"<<endl;
				local_included = false;
				break;
			}
		}
		if(local_included)
		{
			spot_included = true;
		}

		if(self_taken && spot_included)
		{
			// cout<<"ON PREPARE LE SHOW: "<<i<<endl;
			to_replace_id.push_back(i);
		}

		if(!self_taken && spot_included)
		{
			// cout<<"le show est annule"<<endl;
			return;
		}
	}

	if(to_replace_id.size() > 0)
	{
		// cout<<" LE SHOW CEST ICI : "<<endl;
		// custom_show(to_replace_id);
		
		std::sort(to_replace_id.begin(), to_replace_id.end());

		for(int i=(int)to_replace_id.size()-1; i>=0; i--)
		{
			// cout<<"Licensiment dynamique de dernière minute: viré: ";
			// _spots[to_replace_id[i]]->show();
			// cout<<endl;
			// cout<<"Nouveau candidat: ";
			// spot.show();
			// cout<<endl;

			swapop(_spots, to_replace_id[i]);
		}
	}
	// cout<<"   -> ADDED <- "<<endl;
	_rays_id.insert(spot.rays.begin(), spot.rays.end());
	_spots.push_back(make_unique<firemanSpot>(spot));
}

pair<vector<XY>, vector<pair<int, int>>> RaySetSet::to_graph() {
	int size = this->size();

	vector<firemanSpot*> spots;
	vector<XY> nodes(size, {0, 0});
	vector<pair<int, int>> edges;

	for (const unique_ptr<firemanSpot> &spot : this->get_spots()) {
		spots.push_back(spot.get());
	}

	for (int i = 0; i < (int) spots.size(); i++) {
		nodes[i] = spots[i]->position;
	}

	for (int i = 0; i < size; i++) {
		for (int j = i + 1; j < size; j++) {
			unordered_set<rayIndex>* ray_set_1 = &(spots[i]->rays);
			unordered_set<rayIndex>* ray_set_2 = &(spots[j]->rays);

			bool found = false;

			for (rayIndex x : *ray_set_1) {
				for (rayIndex y : *ray_set_2) {
					if (x == y) {
						found = true;
						break;
					}
				}
				if (found) {
					break;
				}
			}

			if (found) {
				edges.push_back({i, j});
			}
		}
	}

	return {nodes, edges};
}

void RaySetSet::finalize()
{
	for(auto ray : _rays_id)
	{
		for(auto& spot : _spots)
		{
			auto good_spot = spot->rays.find(ray);
			if(good_spot != spot->rays.end())
			{
				if(spot->rays.size()>1)
				{
					firemanSpot single_spot;
					single_spot.position = spot->position;
					single_spot.rays.insert(ray);
					auto single_spot_ptr = std::make_unique<firemanSpot>(single_spot);
					_spots.push_back(std::move(single_spot_ptr));
				}
				break;
			}
		}
	}
}

void RaySetSet::show() const
{
	cout<<"{"<<endl;
	for(auto& spot : _spots)
	{
		spot->show();
		cout<<"\tp("<<spot->position.x<<", "<<spot->position.y<<") ";
		// for(auto& ray : spot->rays)
		// {
		// 	cout<<ray<<", ";
		// }
		cout<<endl;
	}
	cout<<"}"<<endl;
}

unsigned int RaySetSet::size() const
{
	return _spots.size();
}

const std::vector<std::unique_ptr<firemanSpot>>& RaySetSet::get_spots() const
{
	return _spots;
}

void RaySetSet::pop_spot(int index)
{
	_spots.erase(_spots.begin() + index);
}



//

void firemanSpot::show() const
{
	std::cout<<"\t";
	for(auto& ray : rays)
	{
		std::cout<<ray<<", ";
	}
}