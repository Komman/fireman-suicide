#include "propagation.hpp"
#include "domination_algorithm.hpp"

using namespace std;

template<typename T>
inline T square(T x)
{
	return x*x;
}

static float distance(const XY& p1, const XY& p2)
{
	return sqrt(float(square(p1.x-p2.x)) + float(square(p1.y-p2.y)));
}

Propagation::Propagation(const Image& i)
	: Image(i.pixels())
{
	if(i.pixels().size() == 0)
	{
		err("Image is empty for propagation");
	}

	_burning = vector<vector<processedInfos>>(i.pixels().size(), vector<processedInfos>(i[0].size(), {false, false, {}, false}));

	for(const auto& f : this->fire_starts())
	{
		_burning[f.x][f.y].burning = true;
	}
	
	this->set_fireman_action();
	this->set_fire_action();
	this->propagate();
}

void Propagation::set_fireman_action()
{
	for(int x=0; x<(int)(_burning.size());x++)
	{
		for(int y=0; y<(int)(pixels()[x].size());y++)
		{
			if(pixels()[x][y] == FIREMAN)
			{
				int max_radius = int(config::fireman_radius())+1;
				for(int dx = -max_radius; dx<=max_radius; dx++)
				{
					for(int dy = -max_radius; dy<=max_radius; dy++)
					{
						if(   x+dx >=0 && x+dx<(int)(_burning.size())
						   && y+dy >=0 && y+dy<(int)(pixels()[x].size())
						   && distance({x+dx, y+dy}, {x, y}) <= config::fireman_radius())
						{
							_burning[x+dx][y+dy].fireman = true;
						}
					}
				}
			}
		}
	}
}


void Propagation::set_fire_action()
{
	int fire_range_max = int(config::fire_radius())+1;
	for(auto fire_pos : this->fire_starts())
	{
		for(int x = std::max(0, fire_pos.x - fire_range_max); x<std::min((int)_pixels.size(), fire_pos.x + fire_range_max); x++)
		{
			for(int y = std::max(0, fire_pos.y - fire_range_max); y<std::min((int)_pixels[x].size(), fire_pos.y + fire_range_max); y++)
			{
				XY potfirepos = {x, y};
				if(distance(potfirepos, fire_pos) <= config::fire_radius())
				{
					_burning[x][y].fire_ranged = true;
				}
			}
		}
	}
}

void Propagation::propagate()
{
	auto copy = this->pixels();

	for(int x=0; x<(int)(_burning.size());x++)
	{
		for(int y=0; y<(int)(pixels()[x].size());y++)
		{
			if(_burning[x][y].fireman)
			{
				(*this)[x][y] = FIREMAN;
			}
		}
	}

	for(int fire_id=0; fire_id < (int)this->fire_starts().size(); fire_id++)
	{
		const auto& f = this->fire_starts()[fire_id];
		for(int i=0; i<config::angle_count(); i++)
		{
			auto rayres = this->compute_ray_indiced(f, i, true);
			for(const auto& p : rayres.burned_pixels)
			{
				// cout<<p<<endl;
				_burning[p.x][p.y].burning = true;
				if(rayres.city_burned)
				{
					// cout<<"BOUHM : "<<config::ray_id(fire_id, i)<<endl;
					_burning[p.x][p.y].ray_passed.push_back(config::ray_id(fire_id, i));
					//custom_show(_burning[p.x][p.y].ray_passed);
					//cout<<p.x<<" "<<p.y<<endl;
				}
			}
		}
	}

	_pixels = std::move(copy);
}

void Propagation::burn_image()
{
	for(unsigned int x=0; x<_burning.size();x++)
	{
		for(unsigned int y=0; y<pixels()[x].size();y++)
		{
			if(_burning[x][y].burning)
			{
				(*this)[x][y] = FIRE;
			}
		}
	}
}


void Propagation::show(bool colored) const
{
	for(unsigned int x=0; x<_burning.size();x++)
	{
		for(unsigned int y=0; y<pixels()[x].size();y++)
		{
			if(colored)
			{	
				if(_burning[x][y].burning && pixels()[x][y]!=FIRE)
				{
					cout<<TERMB::ORANGE;
				}
				else
				{
					if(_burning[x][y].fireman)
					{
						cout<<TERMB::PURPLE;
					}
					else
					{
						cout<<COLORED_SYMBOLS[pixels()[x][y]];
					}
				}
			}
			cout<<SYMBOLS[pixels()[x][y]]<<" ";
			if(colored)
			{
				cout<<TERM::NOCOL;
			}
		}
		cout<<endl;
	}
}


void Propagation::prepare_fireman(RaySetSet& sets)
{
	for(int x=0; x<(int)_pixels.size();x++)
	{
		for(int y=0; y<(int)_pixels[x].size();y++)
		{
			//cout<<x<<" "<<y<<" "<<!_burning[x][y].fire_ranged<<endl;
			if(_pixels[x][y] == FOREST && !_burning[x][y].fire_ranged)
			{
				auto crit = this->get_critical_rays({x, y});
				//custom_show(crit);
				if(crit.size() != 0)
				{
					// cout<<"ALLER HOP HOP HOP ON SE REUNIT LE SHOW VA COMMENCER !!!!"<<endl;
					// print_unordered_set(crit);
					// cout<<"TADAAAA : "<<endl;
					sets.add_spot({{x, y}, crit});
					// cout<<"FIN DU SPECTACLE, MERCI DE VOTRE PARTICIPATION : "<<endl;
				}
			}
		}
	}
	sets.finalize();
}


std::unordered_set<rayIndex> Propagation::get_critical_rays(const XY& position)
{
	std::unordered_set<rayIndex> rays;
	int fireman_radius_int = int(config::fireman_radius()) + 1;

	for(int x = std::max(0, position.x - fireman_radius_int); x<std::min((int)_pixels.size(), position.x + fireman_radius_int); x++)
	{
		for(int y = std::max(0, position.y - fireman_radius_int); y<std::min((int)_pixels[x].size(), position.y + fireman_radius_int); y++)
		{
			XY potfiremanpos = {x, y};
			if(distance(potfiremanpos, position) <= config::fireman_radius())
			{
				//custom_show(_burning[x][y].ray_passed);
				//cout<<x<<" "<<y<<endl;
				for(auto r : _burning[x][y].ray_passed)
				{
					//cout<<r<<endl;
					rays.insert(r);
				}
			}
		}
	}

	return rays;
}

