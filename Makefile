CC=g++
WFLAGS=-Wall -march=native -std=c++17 -O0 -g
LDFLAGS=
EXE=run
ALGORITHMS_FILE="algorithms"

allbin: code

obj/src_graph.o: src/graph.cpp src/graph.hpp
	${CC} ${WFLAGS} -c src/graph.cpp -o obj/src_graph.o

obj/src_image.o: src/image.cpp src/image.hpp src/utils.hpp src/config.hpp
	${CC} ${WFLAGS} -c src/image.cpp -o obj/src_image.o

obj/src_domination_algorithm.o: src/domination_algorithm.cpp src/domination_algorithm.hpp src/graph.hpp
	${CC} ${WFLAGS} -c src/domination_algorithm.cpp -o obj/src_domination_algorithm.o

obj/src_ray_set_set.o: src/ray_set_set.cpp src/ray_set_set.hpp src/utils.hpp
	${CC} ${WFLAGS} -c src/ray_set_set.cpp -o obj/src_ray_set_set.o

obj/src_resolution.o: src/resolution.cpp src/resolution.hpp src/utils.hpp src/config.hpp src/ray_set_set.hpp
	${CC} ${WFLAGS} -c src/resolution.cpp -o obj/src_resolution.o -DCHOOSEN_ALGORITHM=3

obj/src_propagation.o: src/propagation.cpp src/propagation.hpp src/image.hpp src/utils.hpp src/ray_set_set.hpp src/config.hpp
	${CC} ${WFLAGS} -c src/propagation.cpp -o obj/src_propagation.o

obj/src_main.o: src/main.cpp src/image.hpp src/propagation.hpp src/ray_set_set.hpp src/resolution.hpp src/utils.hpp src/config.hpp
	${CC} ${WFLAGS} -c src/main.cpp -o obj/src_main.o -DCHOOSEN_ALGORITHM=3

obj/src_config.o: src/config.cpp src/config.hpp src/utils.hpp
	${CC} ${WFLAGS} -c src/config.cpp -o obj/src_config.o

code: obj  obj/src_graph.o obj/src_image.o obj/src_domination_algorithm.o obj/src_ray_set_set.o obj/src_resolution.o obj/src_propagation.o obj/src_main.o obj/src_config.o
	${CC} ${WFLAGS}   obj/src_graph.o obj/src_image.o obj/src_domination_algorithm.o obj/src_ray_set_set.o obj/src_resolution.o obj/src_propagation.o obj/src_main.o obj/src_config.o  -o ${EXE} ${LDFLAGS} 

obj:
	if [ ! -d obj ]; then mkdir obj;fi

run: code
	./${EXE}

clean:
	rm -rf obj
	rm -rf ${EXE}
	rm -r ${ALGORITHMS_FILE}

allbin: code
	if [ ! -d ${ALGORITHMS_FILE} ]; then mkdir ${ALGORITHMS_FILE};fi
	${CC} ${WFLAGS} -c src/resolution.cpp -o obj/src_resolution.o -DCHOOSEN_ALGORITHM=0
	${CC} ${WFLAGS} -c src/main.cpp -o obj/src_main.o -DCHOOSEN_ALGORITHM=0
	${CC} ${WFLAGS}   obj/src_graph.o obj/src_image.o obj/src_domination_algorithm.o obj/src_ray_set_set.o obj/src_resolution.o obj/src_propagation.o obj/src_main.o obj/src_config.o  -o algorithms/singletons ${LDFLAGS} 

	${CC} ${WFLAGS} -c src/resolution.cpp -o obj/src_resolution.o -DCHOOSEN_ALGORITHM=1
	${CC} ${WFLAGS} -c src/main.cpp -o obj/src_main.o -DCHOOSEN_ALGORITHM=1
	${CC} ${WFLAGS}   obj/src_graph.o obj/src_image.o obj/src_domination_algorithm.o obj/src_ray_set_set.o obj/src_resolution.o obj/src_propagation.o obj/src_main.o obj/src_config.o  -o algorithms/maxfirst ${LDFLAGS} 

	${CC} ${WFLAGS} -c src/resolution.cpp -o obj/src_resolution.o -DCHOOSEN_ALGORITHM=2
	${CC} ${WFLAGS} -c src/main.cpp -o obj/src_main.o -DCHOOSEN_ALGORITHM=2
	${CC} ${WFLAGS}   obj/src_graph.o obj/src_image.o obj/src_domination_algorithm.o obj/src_ray_set_set.o obj/src_resolution.o obj/src_propagation.o obj/src_main.o obj/src_config.o  -o algorithms/smart_maxfirst ${LDFLAGS} 

	${CC} ${WFLAGS} -c src/resolution.cpp -o obj/src_resolution.o -DCHOOSEN_ALGORITHM=3
	${CC} ${WFLAGS} -c src/main.cpp -o obj/src_main.o -DCHOOSEN_ALGORITHM=3
	${CC} ${WFLAGS}   obj/src_graph.o obj/src_image.o obj/src_domination_algorithm.o obj/src_ray_set_set.o obj/src_resolution.o obj/src_propagation.o obj/src_main.o obj/src_config.o  -o algorithms/reduced_maxfirst ${LDFLAGS} 

	${CC} ${WFLAGS} -c src/resolution.cpp -o obj/src_resolution.o -DCHOOSEN_ALGORITHM=4
	${CC} ${WFLAGS} -c src/main.cpp -o obj/src_main.o -DCHOOSEN_ALGORITHM=4
	${CC} ${WFLAGS}   obj/src_graph.o obj/src_image.o obj/src_domination_algorithm.o obj/src_ray_set_set.o obj/src_resolution.o obj/src_propagation.o obj/src_main.o obj/src_config.o  -o algorithms/bruteforce ${LDFLAGS} 

	${CC} ${WFLAGS} -c src/resolution.cpp -o obj/src_resolution.o -DCHOOSEN_ALGORITHM=5
	${CC} ${WFLAGS} -c src/main.cpp -o obj/src_main.o -DCHOOSEN_ALGORITHM=5
	${CC} ${WFLAGS}   obj/src_graph.o obj/src_image.o obj/src_domination_algorithm.o obj/src_ray_set_set.o obj/src_resolution.o obj/src_propagation.o obj/src_main.o obj/src_config.o  -o algorithms/optimforce ${LDFLAGS} 
